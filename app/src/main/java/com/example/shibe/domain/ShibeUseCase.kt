package com.example.shibe.domain

import com.example.shibe.data.model.ShibeRepo
import java.lang.Exception
import javax.inject.Inject

class ShibeUseCase @Inject constructor(private val repo: ShibeRepo) {
    suspend operator fun invoke(): Result<List<String>> {
        return try {
            Result.success(repo.getShibes())
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}