package com.example.shibe.data.model

import android.content.Context
import com.example.shibe.data.model.local.Entity.Shibe
import com.example.shibe.data.model.local.ShibeDatabase
import com.example.shibe.data.model.remote.ShibeService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ShibeRepo @Inject constructor(
    private val service: ShibeService,
    shibeDatabase: ShibeDatabase) {

    private val shibeDao = shibeDatabase.getShibeDao()
    suspend fun getShibes(): List<String> = withContext(Dispatchers.IO) {
        val cashedShibes = shibeDao.getShibeList().map{ it.shibeImageUrl }

        return@withContext cashedShibes.ifEmpty {
            val shibeList = service.getShibes()
            shibeDao.insert(*shibeList.map { Shibe(shibeImageUrl = it) }.toTypedArray())
            return@ifEmpty shibeList
        }
    }
}