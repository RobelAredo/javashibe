package com.example.shibe.data.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.shibe.data.model.local.Dao.ShibeDao
import com.example.shibe.data.model.local.Entity.Shibe
import dagger.Provides

@Database(entities = [Shibe::class], version = 1, exportSchema = false)
abstract class ShibeDatabase: RoomDatabase() {
    abstract fun getShibeDao(): ShibeDao

    companion object {
        const val DATABASE_NAME = "shibe.db"

        @Volatile
        private var instance: ShibeDatabase? = null

        fun getInstance(context: Context): ShibeDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also{ instance = it }
            }
        }

        private fun buildDatabase(context: Context): ShibeDatabase {
            return Room.databaseBuilder(context, ShibeDatabase::class.java, DATABASE_NAME).build()
        }
    }
}