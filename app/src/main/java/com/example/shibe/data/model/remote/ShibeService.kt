package com.example.shibe.data.model.remote

import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {
    @GET("api/shibes")
    suspend fun getShibes(@Query("count") doggieCount: Int = 100): List<String>
}