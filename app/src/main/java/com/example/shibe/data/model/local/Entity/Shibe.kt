package com.example.shibe.data.model.local.Entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Shibe(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val shibeImageUrl: String
)