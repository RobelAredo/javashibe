package com.example.shibe.data.model.local.Dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.shibe.data.model.local.Entity.Shibe

@Dao
interface ShibeDao {
    @Query("SELECT * FROM shibe")
    suspend fun getShibeList(): List<Shibe>

    @Insert
    suspend fun insert(vararg shibeList: Shibe)
}