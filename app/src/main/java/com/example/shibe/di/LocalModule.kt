package com.example.shibe.di

import android.content.Context
import androidx.room.PrimaryKey
import androidx.room.Room
import com.example.shibe.data.model.local.ShibeDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {
    const val DATABASE_NAME = "shibe.db"

    @Volatile
    private var instance: ShibeDatabase? = null

    @Provides
    fun getInstance(@ApplicationContext context: Context): ShibeDatabase {
        return instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also{ instance = it }
        }
    }

    private fun buildDatabase(context: Context): ShibeDatabase {
        return Room.databaseBuilder(context, ShibeDatabase::class.java, DATABASE_NAME).build()
    }
}