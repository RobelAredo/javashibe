package com.example.shibe.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component

@Component(
    modules = [LocalModule::class, RemoteModule::class]
)
interface ShibeComponent {
    @Component.Builder
    interface Builder {
        fun context(@BindsInstance context: Context): Builder
        fun build(): ShibeComponent
    }
}