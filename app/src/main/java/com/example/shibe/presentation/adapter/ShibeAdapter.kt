package com.example.shibe.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.shibe.databinding.ShibeItemBinding

class ShibeAdapter: RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    private var shibeList = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ShibeViewHolder.getInstance(
        ShibeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val shibeImageUrl = shibeList[position]
        holder.loadShibe(shibeImageUrl)
    }

    override fun getItemCount() = shibeList.size

    fun addShibeList(shibes: List<String>) {
        val oldSize = shibeList.size
        shibeList.clear()
        notifyItemRangeChanged(0, oldSize)
        shibeList = shibes.toMutableList()
        notifyItemRangeChanged(0, shibes.size)
    }

    class ShibeViewHolder(private val binding: ShibeItemBinding): RecyclerView.ViewHolder(binding.root) {

        fun loadShibe(shibeImageUrl: String) {
            binding.svShibe.load(shibeImageUrl)
        }

        companion object {
            fun getInstance(binding: ShibeItemBinding) = ShibeViewHolder(binding)
        }
    }
}