package com.example.shibe.presentation.view

import android.content.ContentProvider
import android.content.ContentResolver
import android.content.DialogInterface
import android.os.Bundle
import android.provider.CalendarContract.Attendees.query
import android.provider.ContactsContract
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shibe.databinding.ActivityMainBinding
import com.example.shibe.presentation.adapter.ShibeAdapter
import com.example.shibe.presentation.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity: AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding get() = _binding!!
    private val shibeAdapter by lazy { ShibeAdapter() }
    private val shibeViewModel by viewModels<ShibeViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        setContentView(binding.root)
        initObserver()

        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)!!
        cursor.moveToFirst()
        val st = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
        val contact = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
        Log.d("MAINACTIVITY", contact)
    }

    fun initView() {
        _binding = ActivityMainBinding.inflate(layoutInflater)
        binding.run {
            rvShibeList.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rvShibeList.adapter = shibeAdapter
        }
    }

    private fun initObserver() {
        lifecycleScope.launch {
            shibeViewModel.shibeState.collectLatest { state ->
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    with(state) {
                        if(isLoading) "TODO ADD PROGRESS INDICATOR"
                        else if(errMsg != null) errorMessage(errMsg)
                        else shibeAdapter.addShibeList(shibeList)
                    }
                }
            }
        }
    }

    private fun errorMessage(err: String) {
        AlertDialog.Builder(this@MainActivity)
            .setTitle("Something went wrong!")
            .setMessage(err)
            .setPositiveButton("X", object:DialogInterface.OnClickListener{
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    Toast.makeText(
                        this@MainActivity,
                        "Yo, we see you dog.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
            .show()
    }
}