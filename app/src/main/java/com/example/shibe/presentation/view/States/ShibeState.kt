package com.example.shibe.presentation.view.States

data class ShibeState(
    val isLoading: Boolean = false,
    val errMsg: String? = null,
    val shibeList: List<String> = emptyList()
)