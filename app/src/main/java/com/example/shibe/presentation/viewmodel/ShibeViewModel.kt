package com.example.shibe.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibe.domain.ShibeUseCase
import com.example.shibe.presentation.view.States.ShibeState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShibeViewModel @Inject constructor(private val shibeUseCase: ShibeUseCase): ViewModel() {
    private var _shibeState = MutableStateFlow<ShibeState>(ShibeState(true))
    val shibeState: StateFlow<ShibeState> get() = _shibeState.asStateFlow()

    init {
        viewModelScope.launch {
            val result: Result<List<String>> = shibeUseCase()
            _shibeState.emit(ShibeState(
                errMsg = result.exceptionOrNull()?.message,
                shibeList = result.getOrNull() ?: emptyList()
            ))
        }
    }
}